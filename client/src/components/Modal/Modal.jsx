import React, {useState} from 'react'
import {Button, Form, Message, Modal, TextArea} from 'semantic-ui-react'
import './Modal.css'

const ModalWindow = ({messageId, text, updateMessage, close}) => {
    const [open, setOpen] = React.useState(true)
    const [newReply, setNewReply] = useState('');

    React.useEffect(() => {
        if (!open) {
            close();
        }
    }, [open]);

    const changeMessage = (e) => {
        setNewReply(e.target.value)
    }

    const handleUpdateMessage = () => {
        updateMessage(newReply, messageId)
    }

    return (
        <Modal
            onClose={() => setOpen(false)}
            open={open}
            className={open ? 'reply-modal modal-shown' : 'reply-modal'}
        >
            <Modal.Header>Reply message</Modal.Header>
            <Modal.Content>
                <Modal.Description style={{width: '100%'}}>
                    <Message visible>{text}</Message>
                    <Form>
                        <TextArea
                            className='reply-input'
                            as='textarea'
                            autoFocus={true}
                            type="text"
                            onChange={changeMessage}
                            defaultValue={newReply}
                        />
                    </Form>
                </Modal.Description>
            </Modal.Content>
            <Modal.Actions>
                <Button
                    className='reply-close'
                    color='black'
                    onClick={() => setOpen(false)}
                >
                    Close
                </Button>
                <Button
                    className='reply-button'
                    content="Send"
                    labelPosition='right'
                    icon='checkmark'
                    onClick={() => {
                        handleUpdateMessage();
                        setOpen(false)
                    }}
                    positive
                />
            </Modal.Actions>
        </Modal>
    )
}

export default ModalWindow
