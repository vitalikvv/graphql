import React, {useEffect, useState} from 'react'
import {Button, Comment, Icon} from "semantic-ui-react";
import './OwnMessage.css'
import {Redirect} from "react-router-dom";
import { useCallback } from 'react'

const OwnMessage = ({message, likeMessage, setLikeOrDis, deleteMessage, isLastElement}) => {
    const [isRedirectToMessageEditor, setIsRedirectToMessageEditor] = useState(false)

    const handleUserKeyPress = useCallback((e) => {
        if (e.key === 'ArrowUp' && isLastElement) {
            setIsRedirectToMessageEditor(true)
        }
    }, [isLastElement])

    useEffect(() => {
        window.addEventListener('keydown', handleUserKeyPress);
        return () => {
            window.removeEventListener('keydown', handleUserKeyPress);
        };
    }, [handleUserKeyPress]);

    const clickLikeHandler = () => {

        if (localStorage.getItem(message.id)) {
            setLikeOrDis('dislike');
            likeMessage(message.id, 'dislike');
            return
        }
        setLikeOrDis('like')
        likeMessage(message.id, 'like')
    }

    return (
        <>
            {isRedirectToMessageEditor && <Redirect from='/chat' to='/message_editor'/>}
            <div className='own-message'>
                <Comment.Group>
                    <Comment>
                        <Comment.Content>
                            <Comment.Author className='message-user-name'>#{message.id}</Comment.Author>
                            <Comment.Text className='message-text'>
                                {message.text}
                            </Comment.Text>
                            <Comment.Actions>
                                <Comment.Action
                                    style={{marginRight: 0}}
                                    onClick={clickLikeHandler}
                                    className={message.likesCount > 0 ? 'message-liked' : 'message-like'}><Icon
                                    name='thumbs up'/>
                                </Comment.Action>
                                <Comment.Metadata style={{width: '10px', marginLeft: 0, marginRight: '5px'}}>
                                    <span>{message.likesCount > 0 ? message.likesCount : null}</span>
                                </Comment.Metadata>
                                <Comment.Action
                                    style={{marginRight: 0}}
                                    onClick={clickLikeHandler}
                                    className={message.likesCount > 0 ? 'message-liked' : 'message-like'}><Icon
                                    name='thumbs down'/>
                                </Comment.Action>
                                <Comment.Metadata style={{width: '10px', marginLeft: 0, marginRight: '5px'}}>
                                    <span>{message.likesCount > 0 ? message.likesCount : null}</span>
                                </Comment.Metadata>
                            </Comment.Actions>
                            <Comment.Metadata style={{marginLeft: 0}}>
                                <div className='message-time'>{message.createdAt.split(' ')[1]}</div>
                            </Comment.Metadata>
                        </Comment.Content>
                    </Comment>
                </Comment.Group>
            </div>
        </>
    )
}

export default OwnMessage;