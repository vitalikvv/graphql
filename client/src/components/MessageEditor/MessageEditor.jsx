import React, {useEffect, useState} from 'react'
import './MessageEditor.css'
import {Container, Header, Image, Divider, Form, TextArea, Button} from "semantic-ui-react";
import {useDispatch, useSelector} from "react-redux";
import {Redirect} from "react-router-dom";
import {updateMessage} from "../../store/messagesList";


const MessageEditor = () => {
    const {ownLastMessage} = useSelector(state => ({
        ownLastMessage: state.chat.ownLastMessage,
    }));
    const dispatch = useDispatch();

    const [newMessage, setNewMessage] = useState(ownLastMessage ? ownLastMessage.text : '');
    const [isRedirectToChat, setIsRedirectToChat] = useState(false)

    useEffect(() => {
        if (ownLastMessage) {
            if (Object.keys(ownLastMessage).length === 0) {
                setIsRedirectToChat(true);
            }
        }
        if (!ownLastMessage) {
            setIsRedirectToChat(true);
        }
    }, [ownLastMessage])

    const changeMessage = (e) => {
        setNewMessage(e.target.value)
    }

    const handleUpdateMessage = () => {
        dispatch(updateMessage(newMessage, ownLastMessage.id));
        setIsRedirectToChat(true)
    }

    return (
        <>
            {isRedirectToChat && <Redirect from='/message_editor' to='/chat'/>}
            <div className={'message-editor'}>
                <Container text>
                    <Header as='h2'>Edit your message</Header>
                    <Divider/>
                    <Form style={{display: 'flex', marginBottom: '20px'}}>
                        <Image
                            style={{marginBottom: 0}}
                            src={ownLastMessage ? ownLastMessage.avatar : null}
                            size='small'
                            floated='left'
                        />
                        <TextArea
                            className='edit-message-input'
                            floated='right'
                            as='textarea'
                            autoFocus={true}
                            type="text"
                            onChange={changeMessage}
                            defaultValue={newMessage}
                        />
                    </Form>
                    <Button
                        className='message-input-button'
                        onClick={handleUpdateMessage}
                    >
                        Submit
                    </Button>
                </Container>
            </div>
        </>
    )
}

export default MessageEditor