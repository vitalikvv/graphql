import React, {createRef, useCallback, useEffect, useState} from 'react'
import {Divider, Grid, Header, Icon, Rail, Segment, Sticky, Ref} from 'semantic-ui-react'
import './MessageList.css'
import Message from "../Message/Message";
import OwnMessage from "../OwnMessage/OwnMessage";
import {getMonth, getWeekDay} from "../../helpers/changeDateFormat";
import {useDispatch, useSelector} from "react-redux";
import {addNewMessage,
    deleteMessage,
    likeMessage,
    setLikeOrDis
} from "../../store/messagesList";
import MessageInput from "../MessageInput/MessageInput";


const MessageList = () => {

    const [countOfUsers, setCountOfUsers] = useState(0);
    const [countOfMessages, setCountOfMessages] = useState(0);
    const [lastMessageDate, setLastMessageDate] = useState('');
    const contextRef = createRef()

    const { messages, currentUser, ownLastMessage } = useSelector(state => ({
        messages: state.chat.messages,
        currentUser: state.chat.currentUser,
        ownLastMessage: state.chat.ownLastMessage,
    }));
    const dispatch = useDispatch();

    const dataAnalysis = useCallback(() => {
        if (messages.length > 0) {
            let countOfMessages = 0;
            const set = new Set();
            messages.forEach(message => {
                set.add(message.userId);
                countOfMessages++;
            })
            setCountOfUsers(set.size);
            setCountOfMessages(countOfMessages);
            const lastDate = messages[messages.length - 1].createdAt;
            setLastMessageDate(lastDate);
        } else {
            return
        }
    },[messages])

    useEffect(() => {
        dataAnalysis();
    }, [messages, dataAnalysis])

    const onLikeMessage = React.useCallback((messageId, likeOrDis) => (
        dispatch(likeMessage(messageId, likeOrDis))
    ), [dispatch]);

    const onSetLikeOrDis = React.useCallback((likeOrDis) => (
        dispatch(setLikeOrDis(likeOrDis))
    ), [dispatch]);

    const onAddMessage = React.useCallback((text, currentUser) => (
        dispatch(addNewMessage(text, currentUser))
    ), [dispatch]);

    const onDeleteMessage = React.useCallback((messageId) => (
        dispatch(deleteMessage(messageId))
    ), [dispatch]);

    let date = '';
    const now = new Date();
    const millisecondsInDay = 86400000;

    return (
        <div className='message-list'>
            <Grid centered columns={2}>
                <Grid.Column>
                    <Ref innerRef={contextRef}>
                        <Segment>
                            {messages.map((message) => {

                                    if (date !== message.createdAt.split(' ')[0]) {
                                        date = message.createdAt.split(' ')[0]
                                        const dateParts = message.createdAt.split(' ')[0].split('-');
                                        const dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
                                        const timeDiff = new Date(now) - new Date(dateObject);

                                        let dateToDisplay = getWeekDay(new Date(dateObject)) + `, ${dateParts[0]} ` + getMonth(new Date(dateObject)) + ` ${dateParts[2]}`

                                        if (timeDiff < millisecondsInDay) {
                                            dateToDisplay = 'Today'
                                        }

                                        if (timeDiff >= millisecondsInDay && timeDiff < millisecondsInDay * 2) {
                                            dateToDisplay = 'Yesterday'
                                        }

                                        return (
                                            <div key={message.id}>
                                                <Divider className='messages-divider'
                                                         horizontal>{dateToDisplay}</Divider>
                                                {message.userId === currentUser.userId
                                                    ? <OwnMessage
                                                        likeMessage={onLikeMessage}
                                                        setLikeOrDis={onSetLikeOrDis}
                                                        message={message}
                                                        deleteMessage={onDeleteMessage}
                                                        isLastElement={ownLastMessage ? message.id === ownLastMessage.id : false}
                                                    />
                                                    : <Message
                                                        message={message}
                                                        likeMessage={onLikeMessage}
                                                        setLikeOrDis={onSetLikeOrDis}
                                                    />}
                                            </div>
                                        )
                                    } else {

                                        return (
                                            <div key={message.id}>
                                                {message.userId === currentUser.userId
                                                    ? <OwnMessage
                                                        likeMessage={onLikeMessage}
                                                        setLikeOrDis={onSetLikeOrDis}
                                                        message={message}
                                                        deleteMessage={onDeleteMessage}
                                                        isLastElement={ownLastMessage ? message.id === ownLastMessage.id : false}
                                                    />
                                                    : <Message
                                                        message={message}
                                                        likeMessage={onLikeMessage}
                                                        setLikeOrDis={onSetLikeOrDis}
                                                    />}
                                            </div>
                                        )
                                    }
                                }
                            )}
                            <Rail position='right'>
                                <Sticky
                                    bottomOffset={50}
                                    context={contextRef}
                                    offset={90}
                                >
                                    <div className='right-sticky'>
                                        <Header as='h3'>Сhat statistics</Header>
                                        <div className='header-users-count'>
                                            <Icon name='user'/>
                                            {countOfUsers} participants
                                        </div>
                                        <Divider/>
                                        <div className='header-messages-count'>
                                            <Icon name='mail'/>
                                            {countOfMessages} messages
                                        </div>
                                        <Divider/>
                                        <div className='header-last-message-date'>
                                            <Icon name='calendar'/>
                                            Last message at:
                                            <br/>
                                            {lastMessageDate}
                                        </div>
                                    </div>
                                </Sticky>
                            </Rail>
                        </Segment>
                    </Ref>
                    <MessageInput
                        addNewMessage={onAddMessage}
                        currentUser={currentUser}
                    />
                </Grid.Column>
            </Grid>
        </div>
    )
}

export default MessageList