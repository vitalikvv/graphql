import * as axios from "axios";

const Instance = axios.create({
    baseURL: 'http://localhost:3000/',
    credentials: true
});

export const getAuthUser = (payload) => {
    return Instance.post(`auth/`, payload)
        .then(response => {
            return response
        })
        .catch((error) => {
            throw error.response.data.message;
        });
}

export function getMessages() {
    return Instance.get(`messages/`)
        .then(response => {
            return response;
        })
        .catch((error) => {
            throw error;
        });
}

export function newMessage(payload) {
    return Instance.post(`messages/`, payload)
        .then(response => {
            return response;
        })
}

export function updateMessageById(payload) {
    return Instance.put(`messages/`, payload)
        .then(response => {
            return response;
        })
}

export function deleteMessageById(payload) {
    return Instance.delete(`messages/:${payload}`)
        .then(response => {
            return response;
        })
}

export function getUsers() {
    return Instance.get(`users/`)
        .then(response => {
            return response;
        })
        .catch((error) => {
            throw error;
        });
}

export function updateOrCreateUser(payload) {
    return Instance.put(`users/`, payload)
        .then(response => {
            return response;
        })
}

export function deleteUserById(payload) {
    return Instance.delete(`users/:${payload}`)
        .then(response => {
            return response;
        })
}