
export const getRandomFoto = async () => {
    const url = 'https://source.unsplash.com/random/800x800/?face';
    const response = await fetch(url);
    return response.url
}
