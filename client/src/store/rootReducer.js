import {combineReducers} from 'redux'
import {chat} from "./messagesList";

export const rootReducer = combineReducers({
    chat
})