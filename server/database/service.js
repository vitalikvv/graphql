import {v4 as uuidv4} from 'uuid';
import {users} from "./database.js"
import {messages} from "./database.js";

export const getAuthData = (userData) => {
    const { login, password } = userData;

    users.forEach(u => u.user === login);
    return users.find(u => u.user === login && u.password === password);
}

export const updateUser = (userData) => {
    const { userId } = userData;
    users.forEach(function(el, index, array) {
        if (userId === array[index].userId) {
            array[index] = userData
        }
    });
}

export const createUser = (userData) => {
    const userId = uuidv4();
    const password = '123';
    const newUser = {...userData, userId, password};
    users.push(newUser);
}

export const deleteUser = (userId) => {
    const arr = userId.split(':')
    let removeIndexElement
    users.forEach(function(el, index, array) {
        if (arr[1] === array[index].userId) {
            removeIndexElement = index
        }
    });
    users.splice(removeIndexElement, 1);
}

export const createMessage = (messageData) => {
    const {text, currentUser} = messageData;
    const {userId, avatar, user} = currentUser;
    const tzoffset = (new Date()).getTimezoneOffset() * 60000;
    const createdDate = (new Date(Date.now() - tzoffset)).toISOString();
    const message = {
        text: text,
        editedAt: '',
        id: messages.length + 1,
        createdAt: createdDate,
        likesCount: 0,
        userId,
        avatar,
        user
    }
    messages.push(message);
}

export const updateMessage = (payload) => {
    const { newMessageText, messageId, editedDate } = payload;
    messages.forEach(function(el, index, array) {
        if (messageId === array[index].id) {
            array[index].text = newMessageText;
            array[index].editedAt = editedDate;
        }
    });
}

export const deleteMessage = (messageId) => {
    const arr = messageId.split(':')
    let removeIndexElement
    messages.forEach(function(el, index, array) {
        if (arr[1] === array[index].id) {
            removeIndexElement = index
        }
    });
    messages.splice(removeIndexElement, 1);
}