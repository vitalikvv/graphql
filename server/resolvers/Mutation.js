function postMessage(parent, args, context, info) {
    return context.prisma.createMessage ({
        text: args.text,
    })
}

module.exports = {
    postMessage
}
